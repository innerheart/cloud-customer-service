package com.igeekhome.dao;

import com.igeekhome.pojo.WorkloadStatistics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 工作量统计 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface WorkloadStatisticsMapper extends BaseMapper<WorkloadStatistics> {
    @Select("SELECT SUM(totalMessageCount) FROM workload_statistics ${ew.customSqlSegment}")
    int getTotalMessageCountSum(@Param(Constants.WRAPPER) Wrapper wrapper);

    @Select("SELECT SUM(totalSessionCount) FROM workload_statistics ${ew.customSqlSegment}")
    int getTotalSessionCountSum(@Param(Constants.WRAPPER) Wrapper wrapper);

    @Select("SELECT SUM(totalEffectiveSessionCount) FROM workload_statistics ${ew.customSqlSegment}")
    int getTotalEffectiveSessionCountSum(@Param(Constants.WRAPPER) Wrapper wrapper);

    @Select("SELECT SUM(totalEndSessionCount) FROM workload_statistics ${ew.customSqlSegment}")
    int getTotalEndSessionCountSum(@Param(Constants.WRAPPER) Wrapper wrapper);

    @Select("SELECT COUNT(totalSessionTime) FROM workload_statistics ${ew.customSqlSegment}")
    int getTotalSessionTimeNum(@Param(Constants.WRAPPER) Wrapper wrapper);
}
