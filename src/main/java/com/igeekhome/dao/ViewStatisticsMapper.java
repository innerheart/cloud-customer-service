package com.igeekhome.dao;

import com.igeekhome.pojo.ViewStatistics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 视图统计 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface ViewStatisticsMapper extends BaseMapper<ViewStatistics> {
    @Select("SELECT pageView FROM view_statistics ${ew.customSqlSegment}")
    int[] getPageViewCol(@Param(Constants.WRAPPER) Wrapper wrapper);

    @Select("SELECT visitorNum FROM view_statistics ${ew.customSqlSegment}")
    int[] getVisitorNumCol(@Param(Constants.WRAPPER) Wrapper wrapper);

    @Select("SELECT visitorCount FROM view_statistics ${ew.customSqlSegment}")
    int[] getVisitorCountCol(@Param(Constants.WRAPPER) Wrapper wrapper);

}
