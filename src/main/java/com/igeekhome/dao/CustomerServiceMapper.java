package com.igeekhome.dao;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.toolkit.Constants;
import com.igeekhome.pojo.CustomerService;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 客服表 Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface CustomerServiceMapper extends BaseMapper<CustomerService> {
    @Select("SELECT COUNT(*) FROM customer_service ${ew.customSqlSegment}")
    int getQueryAnswer(@Param(Constants.WRAPPER) Wrapper wrapper);
}
