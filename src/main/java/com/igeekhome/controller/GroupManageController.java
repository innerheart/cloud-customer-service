package com.igeekhome.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeekhome.biz.IGroupManageService;
import com.igeekhome.pojo.CustomerService;
import com.igeekhome.pojo.GroupManage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * 组管理 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
@RequestMapping("/groupManage")
public class GroupManageController {
    @Autowired
    private IGroupManageService iGroupManageService;

    @RequestMapping("/index")
    public String index(Model model, HttpSession session, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        GroupManage gm = iGroupManageService.getById(13);
        model.addAttribute("gm",gm);
        model.addAttribute("current",current);
        initgroupmanage(model,current,size);
        CustomerService customerService = (CustomerService) session.getAttribute("cs");
        session.setAttribute("userRealName", customerService.getRealname());
        return "groupManage";
    }
    @RequestMapping("/update")
    public String update(Model model,GroupManage groupManage, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        // 根据id将对像查询
        groupManage= this.iGroupManageService.getById(groupManage.getGroupid());
        modelCarry(model, groupManage, current, size);
        return  "updateGroupManage";
    }
    @RequestMapping("/updateSubmit")
    public String updateSubmit(Model model,GroupManage groupManage, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        this.iGroupManageService.updateById(groupManage);
        modelCarry(model, groupManage, current, size);
        initgroupmanage(model, current, size);
        return "groupManage";
    }
    @RequestMapping("/add")
    public String add(Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        GroupManage groupManage = new GroupManage();
        modelCarry(model,groupManage,current,size);
        initgroupmanage(model, current, size);
        return "addGroupManage";
    }
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model,GroupManage groupManage,@RequestParam("current") Integer current,@RequestParam("size") Integer size)
    {
        this.iGroupManageService.save(groupManage);
        modelCarry(model, groupManage, current, size);
        initgroupmanage(model,current,size);
        return "groupManage";
    }
    @RequestMapping("/select")
    public String select(Model model,GroupManage groupManage)
    {
        groupManage= this.iGroupManageService.getById(groupManage.getGroupid());
        if (groupManage == null)
        {
            return "selectErrorGroupManage";
        }
        model.addAttribute("gm", groupManage);
        return "selectResultGroupManage";
    }

    @RequestMapping("/del")
    public String del(Model model,GroupManage groupManage,@RequestParam("current") Integer current,@RequestParam("size") Integer size)
    {

        this.iGroupManageService.removeById(groupManage.getGroupid());
        modelCarry(model, groupManage, current, size);
        initgroupmanage(model,current,size);
        return  "groupManage";
    }
    public void initgroupmanage(Model model, Integer current, Integer size)
    {

        IPage<GroupManage> page=new Page<>(current,size);
        IPage<GroupManage> page1 = this.iGroupManageService.page(page);

        List<GroupManage> list = page1.getRecords();
        long pagesCount = page1.getPages();


        model.addAttribute("list",list);
        model.addAttribute("pagesCount",pagesCount);
    }
    public void modelCarry(Model model, GroupManage groupManage, Integer current, Integer size)
    {
        model.addAttribute("gm",groupManage);
        model.addAttribute("current",current);
        model.addAttribute("size",size);
    }

}

