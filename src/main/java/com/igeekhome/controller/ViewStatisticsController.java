package com.igeekhome.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeekhome.biz.IViewStatisticsService;
import com.igeekhome.pojo.CustomerService;
import com.igeekhome.pojo.ViewStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * 视图统计 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
@RequestMapping("/viewStatistics")
public class ViewStatisticsController {


    @Autowired
    private IViewStatisticsService iViewStatisticsService;

    @RequestMapping("/index")
    public String toAttendanceStatisticsIndex(HttpSession session, Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        IPage<ViewStatistics> iPage = new Page<>(current, size);
        iPage = iViewStatisticsService.page(iPage);
        List<ViewStatistics> list = iPage.getRecords();
        model.addAttribute("lists", list);
        CustomerService customerService = (CustomerService) session.getAttribute("cs");
        model.addAttribute("pagecount", iPage.getPages());
        session.setAttribute("userRealName", customerService.getRealname());
        //int[] list1 = iViewStatisticsService.getPageViewCol(new QueryWrapper());
        model.addAttribute("pageView", iViewStatisticsService.getPageViewCol(new QueryWrapper()));
        model.addAttribute("visitorNum", iViewStatisticsService.getVisitorNumCol(new QueryWrapper()));
        model.addAttribute("visitorCount", iViewStatisticsService.getVisitorCountCol(new QueryWrapper()));
        return "viewStatistics";
    }

}

