package com.igeekhome.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeekhome.biz.INoticeService;
import com.igeekhome.pojo.GroupManage;
import com.igeekhome.pojo.Notice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 通知表 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
@RequestMapping("/notice")
public class NoticeController {
    @Autowired
    private INoticeService iNoticeService;

    @RequestMapping("/index")
    public String index(Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size)

    {
        Notice gm =iNoticeService.getById(18);
        model.addAttribute("gm",gm);
        model.addAttribute("current",current);
        initNoticeService(model,current,size);
        return "notice.html";
    }
    @RequestMapping("/up") //Model model,Notice notice,@RequestParam("current") Integer current,@RequestParam("size") Integer size
    public String update(Model model,Notice notice,@RequestParam("current") Integer current,@RequestParam("size") Integer size)
    //public String update(Model model,Notice notice)
    {
        // 根据id将对像查询
        notice= this.iNoticeService.getById(notice.getId());

        model.addAttribute("main","消息修改");
        model.addAttribute("notice",notice);
        model.addAttribute("current",current);
        model.addAttribute("size",size);
        return  "updatenotice"; // 到修改页面
    }

    @RequestMapping("/updateSubmit") //修改表单
    public String updateSubmit(Model model,Notice notice,@RequestParam("current") Integer current,@RequestParam("size") Integer size)
    {
        this.iNoticeService.updateById(notice);
        modelCarry(model, notice, current, size);
        initNoticeService(model,current,size);
        return "notice";
    }

    @RequestMapping("/add")
    public String add(Model model,Notice notice)
    {
        model.addAttribute("main","消息增加");
        return  "addnotice";
    }

    @RequestMapping("/addSubmit")
    public String addsubmit(Model model,Notice notice,@RequestParam("current") Integer current,@RequestParam("size") Integer size)
    {
        this.iNoticeService.save(notice);
        model.addAttribute("gm",notice);
        initNoticeService(model,current,size);
        return "notice";
    }
    @RequestMapping("/select")
    public String select(Model model,Notice notice)
    {
        notice = this.iNoticeService.getById(notice.getId());
        if (notice == null)
        {
            return "selectErrorNotice";
        }
        model.addAttribute("gm",notice);
        return "selectnotice";
    }

    @RequestMapping("/del")
    public String del(Model model,Notice notice,@RequestParam("current") Integer current,@RequestParam("size") Integer size)
    {
        this.iNoticeService.removeById(notice.getId());
        model.addAttribute("gm",notice);
        initNoticeService(model,current,size);
        return  "notice";
    }


    public void initNoticeService(Model model, Integer current, Integer size)
    {

        IPage<Notice> page=new Page<>(current,size);
        IPage<Notice> page1 = this.iNoticeService.page(page);

        List<Notice> list = page1.getRecords();
        long pagesCount = page1.getPages();


        model.addAttribute("list",list);
        model.addAttribute("pagesCount",pagesCount);

    }
    public void modelCarry(Model model, Notice notice, Integer current, Integer size)
    {
        model.addAttribute("gm",notice);
        model.addAttribute("current",current);
        model.addAttribute("size",size);
    }

}

