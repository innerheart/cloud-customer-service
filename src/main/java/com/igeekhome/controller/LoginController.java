package com.igeekhome.controller;

import com.igeekhome.biz.ICustomerInfoService;
import com.igeekhome.biz.ICustomerServiceService;
import com.igeekhome.biz.INoticeService;
import com.igeekhome.pojo.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @Autowired
    private ICustomerServiceService iCustomerServiceService;
    @Autowired
    private ICustomerInfoService iCustomerInfoService;
    @Autowired
    private INoticeService iNoticeService;

    @RequestMapping("/")
    public String Login()
    {
        return "login";
    }

    @RequestMapping("index")
    public String ToIndex(Model model, HttpSession session)
    {
        model.addAttribute("customerServiceNumber", iCustomerServiceService.count());
        model.addAttribute("customerInfoNumber", iCustomerInfoService.count());
        model.addAttribute("noticeNumber", iNoticeService.count());

        CustomerService cs = (CustomerService) session.getAttribute("cs");
        session.setAttribute("userRealName", cs.getRealname());

        return "index";
    }

    @RequestMapping("/login")
    public String ToLogin()
    {
        return "login";
    }

    @RequestMapping("/exit")
    public String ToExit(HttpSession session)
    {
        session.removeAttribute("cs");
        return "login";
    }


}
