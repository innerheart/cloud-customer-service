package com.igeekhome.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeekhome.biz.IBlackListService;
import com.igeekhome.pojo.BlackList;
import com.igeekhome.pojo.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 恶意用户列表 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
@RequestMapping("/blackList")
public class BlackListController {

    @Autowired
    private IBlackListService iBlackListService;

    //跳转到黑名单初始界面
    @RequestMapping("/index")
    public String index(Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size) {
        initBlackListService(model, current, size);
        return "blacklist";
    }


    //跳转到更新页面
    @RequestMapping("/update")
    public String update(Model model, BlackList blackList, @RequestParam("current") Integer current, @RequestParam("size") Integer size) {
        // 根据id将对像查询
        QueryWrapper<BlackList> queryWrapper = new QueryWrapper<BlackList>();
        queryWrapper.eq("customerId", blackList.getCustomerid());
        blackList = this.iBlackListService.getOne(queryWrapper);
        //传值并跳转到黑名单更新界面
        model.addAttribute("bl", blackList);
        model.addAttribute("main", "黑名单信息修改");
        model.addAttribute("current", current);
        model.addAttribute("size", size);
        return "blacklistupdate";
    }

    //更改并返回当前页面
    @RequestMapping(value = "/updateSubmit", method = RequestMethod.POST)
    public String updateSubmit(Model model, BlackList blackList, @RequestParam("current") Integer current, @RequestParam("size") Integer size) {
        //根据黑名单客户id更新信息
        QueryWrapper<BlackList> queryWrapper = new QueryWrapper<BlackList>();
        queryWrapper.eq("customerId", blackList.getCustomerid());
        iBlackListService.update(blackList, queryWrapper);
        //返回黑名单初始界面
        initBlackListService(model, current, size);
        return "blacklist";
    }

    //跳转到添加页面
    @RequestMapping("/add")
    public String add(Model model, HttpSession session) {
        model.addAttribute("main", "黑名单信息添加");
        BlackList blackList=new BlackList();
        CustomerService customerService = (CustomerService) session.getAttribute("cs");
        model.addAttribute("id", customerService.getCustomerserviceid());
        model.addAttribute("blackList", blackList);
        return "blacklistadd";
    }

    //添加并返回黑名单首页
    @RequestMapping(value = "/addSubmit", method = RequestMethod.POST)
    public String addSubmit(Model model, BlackList blackList, HttpSession session, @RequestParam("current") Integer current, @RequestParam("size") Integer size) {
        //对表单数据进行验证
        QueryWrapper<BlackList> queryWrapper = new QueryWrapper<BlackList>();
        queryWrapper.eq("customerId", blackList.getCustomerid());
        int i = iBlackListService.count(queryWrapper);

        //用户已加入黑名单
        if (i != 0) {
            //表单重要信息未填，验证不成功，返回表单提交界面，将已填数据传回，并提示完善信息
            CustomerService customerService = (CustomerService) session.getAttribute("cs");
            model.addAttribute("id", customerService.getCustomerserviceid());

            model.addAttribute("blackList", blackList);
            model.addAttribute("alert", "该用户信息已存在");
            model.addAttribute("main", "黑名单信息添加");
            return "blacklistadd";
        }
        //添加黑名单创建日期并保存到数据库
        LocalDateTime dateTime = LocalDateTime.now();
        blackList.setTime(dateTime);
        iBlackListService.save(blackList);
        initBlackListService(model, current, size);
        return "blacklist";
    }


    //删除并返回当前页面
    @RequestMapping("/delete")
    public String delete(Model model, BlackList blackList, @RequestParam("current") Integer current, @RequestParam("size") Integer size) {
        QueryWrapper<BlackList> queryWrapper = new QueryWrapper<BlackList>();
        queryWrapper.eq("customerId", blackList.getCustomerid());
        iBlackListService.remove(queryWrapper);

        initBlackListService(model, current, size);
        return "blacklist";
    }

    //查询
    @RequestMapping("/select")
    public String select(Model model, HttpServletRequest request, @RequestParam("current") Integer current, @RequestParam("size") Integer size) {
        //对查询信息添加到QueryWrapper
        QueryWrapper<BlackList> queryWrapper = new QueryWrapper<BlackList>();
        String customerServiceId = request.getParameter("customerserviceid");
        if (customerServiceId != null && customerServiceId != "")
            queryWrapper.eq("customerServiceId", customerServiceId);
        String customerId = request.getParameter("customerid");
        if (customerId != null && customerId != "")
            queryWrapper.eq("customerId", customerId);

        //分页加载查询结果黑名单界面
        IPage<BlackList> page = new Page<>(current, size);
        IPage<BlackList> page1 = this.iBlackListService.page(page, queryWrapper);

        List<BlackList> list = page1.getRecords();
        long pagesCount = page1.getPages();

        model.addAttribute("list", list);
        model.addAttribute("pagesCount", pagesCount);
        model.addAttribute("main", "黑名单查询结果");
        model.addAttribute("customerserviceid", customerServiceId);
        model.addAttribute("customerid", customerId);

        return "blacklistselect";
    }

    //分页加载初始化黑名单界面
    public void initBlackListService(Model model, Integer current, Integer size) {

        IPage<BlackList> page = new Page<>(current, size);
        IPage<BlackList> page1 = this.iBlackListService.page(page);

        List<BlackList> list = page1.getRecords();
        long pagesCount = page1.getPages();


        model.addAttribute("list", list);
        model.addAttribute("pagesCount", pagesCount);
        model.addAttribute("main", "黑名单管理主页");

    }

}

