package com.igeekhome.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeekhome.biz.IWorkLogService;
import com.igeekhome.pojo.CustomerService;
import com.igeekhome.pojo.RoleManage;
import com.igeekhome.pojo.WorkLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * <p>
 * 工作日志 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
@RequestMapping("/workLog")
public class WorkLogController {

    @Autowired
    private IWorkLogService iWorkLogService;

    @RequestMapping("/index")
    public String index(Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size) {
        WorkLog wl = iWorkLogService.getById(18);
        model.addAttribute("wl",wl);
        initworklog(model, current, size);
        return "workLog.html";
    }
    @RequestMapping("/update")
//    public String update(Model model, WorkLog wl, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    public String update(Model model, WorkLog wl)

    {
        // 根据id将对像查询
        wl= this.iWorkLogService.getById(wl.getId());
        model.addAttribute("wl",wl);
        model.addAttribute("main","工作日志修改");
//        model.addAttribute("current",current);
//        model.addAttribute("size",size);
        return  "updateworklog";
    }
    @RequestMapping("/updateSubmit")
    public String updateSubmit(Model model, WorkLog wl, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        this.iWorkLogService.updateById(wl);
        model.addAttribute("wl",wl);
        initworklog(model,current,size);
        return "workLog";

    }
    @RequestMapping("/add")
    public String add(Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        WorkLog workLog = new WorkLog();
        modelCarry(model,workLog,current,size);
        initworklog(model, current, size);
        return "addworklog";
    }
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model,WorkLog workLog,@RequestParam("current") Integer current,@RequestParam("size") Integer size)
    {
        this.iWorkLogService.save(workLog);
        modelCarry(model, workLog, current, size);
        initworklog(model,current,size);
        return "workLog";
    }
    @RequestMapping("/select")
    public String select(Model model,WorkLog wl)
    {
        wl= this.iWorkLogService.getById(wl.getId());
        if (wl == null)
        {
            return "selectErrorworklog";
        }
        model.addAttribute("wl", wl);
        return "selectResultworklog";
    }
    @RequestMapping("/del")
    public String del(Model model,WorkLog wl,@RequestParam("current") Integer current,@RequestParam("size") Integer size)
    {
        this.iWorkLogService.removeById(wl.getId());
        //   model.addAttribute("wl",wl);
        modelCarry(model,wl, current, size);

        initworklog(model,current,size);
        return  "workLog";
    }

    public void initworklog(Model model, Integer current, Integer size) {
        IPage<WorkLog> page = new Page<>(current, size);
        IPage<WorkLog> page1 = this.iWorkLogService.page(page);
        List<WorkLog> list = page1.getRecords();
        long pagesCount = page1.getPages();
        model.addAttribute("lists", list);
        model.addAttribute("pagesCount", pagesCount);
    }
    public void modelCarry(Model model, WorkLog wl, Integer current, Integer size)
    {
        model.addAttribute("wl",wl);
        model.addAttribute("current",current);
        model.addAttribute("size",size);
    }

}
