package com.igeekhome.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.igeekhome.biz.ICustomerInfoService;
import com.igeekhome.biz.ICustomerServiceService;
import com.igeekhome.pojo.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

@Controller
public class SignUpController {

    @Autowired
    private ICustomerServiceService iCustomerServiceService;

    @RequestMapping("/signUp")
    public String ToRegister()
    {
        return "signup";
    }

    @RequestMapping("/RegisterToIndex")
    public String RegisterToIndex(Model model, CustomerService customerService, HttpSession session)
    {
        String path = "";
        QueryWrapper<CustomerService> queryWrapper = new QueryWrapper();
        queryWrapper.eq("nickName", customerService.getNickname());
        queryWrapper.eq("realName", customerService.getRealname());
        queryWrapper.eq("email", customerService.getEmail());
        int count = iCustomerServiceService.getQueryAnswer(queryWrapper);

        QueryWrapper queryWrapper1 = new QueryWrapper();
        queryWrapper1.eq("email", customerService.getEmail());
        int count1 = iCustomerServiceService.getQueryAnswer(queryWrapper1);

        System.out.println("count = " + count);
        if(count >= 1 || count1 >= 1) // signUp --> model.msg
        {
            model.addAttribute("msg", "该账户已经存在！");
            path = "signup";
        }
        else // login --> session.msg
        {
            iCustomerServiceService.save(customerService);
            session.setAttribute("msg", "注册成功！");
            path = "login";
        }

        return path;
    }
}
