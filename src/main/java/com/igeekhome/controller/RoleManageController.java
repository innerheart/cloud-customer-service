package com.igeekhome.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeekhome.biz.IRoleManageService;
import com.igeekhome.pojo.CustomerService;
import com.igeekhome.pojo.RoleManage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * 角色管理 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
@RequestMapping("/roleManage")
public class RoleManageController {
    @Autowired
    private IRoleManageService iRoleManageService;

    @RequestMapping("/index")
    public String index(Model model, HttpSession session, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        RoleManage rm =iRoleManageService.getById(11);
        model.addAttribute("rm",rm);
        model.addAttribute("current",current);
        initrolemanage(model,current,size);
        CustomerService customerService = (CustomerService) session.getAttribute("cs");
        session.setAttribute("userRealName", customerService.getRealname());
        return "roleManage";
    }
    @RequestMapping("/update")
    public String update(Model model, RoleManage roleManage,@RequestParam("current") Integer current,@RequestParam("size") Integer size)
    {
        // 根据id将对像查询
        roleManage= this.iRoleManageService.getById(roleManage.getRoleid());
        modelCarry(model, roleManage, current, size);
        return  "updateRoleManage";
    }
    @RequestMapping("/updateSubmit")
    public String updateSubmit(Model model,RoleManage roleManage, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        this.iRoleManageService.updateById(roleManage);
        modelCarry(model, roleManage, current, size);
        initrolemanage(model, current, size);
        return "roleManage";
    }

    @RequestMapping("/add")
    public String add(Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        RoleManage roleManage = new RoleManage();
        modelCarry(model,roleManage,current,size);
        initrolemanage(model, current, size);
        return "addRoleManage";
    }
    @RequestMapping("/addSubmit")
    public String addSubmit(Model model,RoleManage roleManage,@RequestParam("current") Integer current,@RequestParam("size") Integer size)
    {
        this.iRoleManageService.save(roleManage);
        modelCarry(model, roleManage, current, size);
        initrolemanage(model,current,size);
        return "roleManage";
    }

    @RequestMapping("/select")
    public String select(Model model,RoleManage roleManage)
    {
        roleManage= this.iRoleManageService.getById(roleManage.getRoleid());
        if (roleManage == null)
        {
            return "selectErrorRoleManage";
        }
        model.addAttribute("rm", roleManage);
        return "selectResultRoleManage";
    }

    @RequestMapping("/del")
    public String del(Model model,RoleManage roleManage,@RequestParam("current") Integer current,@RequestParam("size") Integer size)
    {

        this.iRoleManageService.removeById(roleManage.getRoleid());
        modelCarry(model, roleManage, current, size);
        initrolemanage(model,current,size);
        return  "roleManage";
    }
    public void initrolemanage(Model model, Integer current, Integer size)
    {

        IPage<RoleManage> page=new Page<>(current,size);
        IPage<RoleManage> page1 = this.iRoleManageService.page(page);

        List<RoleManage> list = page1.getRecords();
        long pagesCount = page1.getPages();


        model.addAttribute("list",list);
        model.addAttribute("pagesCount",pagesCount);

    }
    public void modelCarry(Model model, RoleManage roleManage, Integer current, Integer size)
    {
        model.addAttribute("rm",roleManage);
        model.addAttribute("current",current);
        model.addAttribute("size",size);
    }

}

