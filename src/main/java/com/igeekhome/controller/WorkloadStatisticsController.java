package com.igeekhome.controller;



import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeekhome.biz.IWorkloadStatisticsService;
import com.igeekhome.pojo.CustomerService;
import com.igeekhome.pojo.WorkloadStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * 工作量统计 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
@RequestMapping("/workloadStatistics")
public class WorkloadStatisticsController {


    @Autowired
    private IWorkloadStatisticsService iWorkloadStatisticsService;

    @RequestMapping("/index")
    public String toWorkloadStatisticsIndex(HttpSession session, Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        IPage<WorkloadStatistics> iPage = new Page<>(current, size);
        iPage = iWorkloadStatisticsService.page(iPage);
        List<WorkloadStatistics> list = iPage.getRecords();
        model.addAttribute("lists", list);
        CustomerService customerService = (CustomerService) session.getAttribute("cs");
        model.addAttribute("pagecount", iPage.getPages());
        session.setAttribute("userRealName", customerService.getRealname());
        model.addAttribute("TotalMessageCountSum", iWorkloadStatisticsService.getTotalMessageCountSum(null));
        model.addAttribute("TotalSessionCountSum", iWorkloadStatisticsService.getTotalSessionCountSum(null));
        model.addAttribute("TotalEffectiveSessionCountSum", iWorkloadStatisticsService.getTotalEffectiveSessionCountSum(null));
        model.addAttribute("TotalEndSessionCountSum", iWorkloadStatisticsService.getTotalEndSessionCountSum(null));

        int[] a = new int[8];
        int index = 0;
        int sum = 0;
        for(int i = 0; i < 8000; i += 1000)
        {
            QueryWrapper<WorkloadStatistics> qw = new QueryWrapper();
            qw.le("totalSessionTime", i+1000);
            a[index++] = iWorkloadStatisticsService.getTotalSessionTimeNum(qw) - sum;
            sum = iWorkloadStatisticsService.getTotalSessionTimeNum(qw);
        }
        model.addAttribute("a", a);
        return "workloadStatistics";
    }
}

