package com.igeekhome.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeekhome.biz.ISystemMessageService;
import com.igeekhome.pojo.SystemMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 系统信息 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
@RequestMapping("/systemMessage")
public class SystemMessageController {
    @Autowired
    private ISystemMessageService iSystemMessageService;

    @RequestMapping("/index")
    public String index(Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        SystemMessage sm = iSystemMessageService.getById(1);
        model.addAttribute("sm",sm);
        initsystemMessage(model,current,size);
        return "systemMessage";
    }

    @RequestMapping("/delete")
    public String SystemMessageDelete(Model model, SystemMessage systemMessage, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        iSystemMessageService.removeById(systemMessage.getId());
        List<SystemMessage> list = iSystemMessageService.list();
        model.addAttribute("lists", list);
        initsystemMessage(model,current,size);
        return "systemMessage";
    }
    @RequestMapping("/update")
    public String SystemMessageupdate(Model model, SystemMessage sm, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        sm=this.iSystemMessageService.getById(sm.getId());
        model.addAttribute("sm",sm);
        model.addAttribute("alertModify","系统消息修改");
        return  "updatesystemMessage";
    }

    @RequestMapping("/updateSubmit")
    public String SystemMessageupdateSubmit(Model model, SystemMessage sm, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        this.iSystemMessageService.updateById(sm);
        model.addAttribute("sm",sm);
        initsystemMessage(model,current,size);
        return "systemMessage";
    }

    @RequestMapping("/add")
    public String SystemMessageadd(Model model,SystemMessage sm) {
//        sm=this.iSystemMessageService.getById(sm.getId());
        model.addAttribute("sm",sm);
        model.addAttribute("alertAdd", "系统消息添加");
        return "addsystemMessage";
    }

    @RequestMapping("/addSubmit")
    public String SystemMessageupdateaddSubmit(Model model, SystemMessage sm, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        LocalDateTime dateTime = LocalDateTime.now();
        sm.setCreatetime(dateTime);

        this.iSystemMessageService.save(sm);
        model.addAttribute("sm",sm);
        initsystemMessage(model,current,size);
        return "systemMessage";
    }

    @RequestMapping("/select")
    public String select(Model model, SystemMessage sm)
    {
        sm= this.iSystemMessageService.getById(sm.getId());

        model.addAttribute("sm", sm);
        return "selectResultSystemMessage";

    }

    public void initsystemMessage(Model model, Integer current, Integer size)
    {

        IPage<SystemMessage> page=new Page<>(current,size);
        IPage<SystemMessage> page1 = this.iSystemMessageService.page(page);

        List<SystemMessage> list = page1.getRecords();
        long pagesCount = page1.getPages();


        model.addAttribute("lists",list);
        model.addAttribute("pagesCount",pagesCount);

    }
//    public void modelCarry(Model model, SystemMessage sm, Integer current, Integer size)
//    {
//        model.addAttribute("sm",sm);
//        model.addAttribute("current",current);
//        model.addAttribute("size",size);
//    }



}

