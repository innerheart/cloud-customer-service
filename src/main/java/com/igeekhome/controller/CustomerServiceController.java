package com.igeekhome.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeekhome.biz.ICustomerInfoService;
import com.igeekhome.biz.ICustomerServiceService;
import com.igeekhome.biz.INoticeService;
import com.igeekhome.pojo.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpSession;
import java.util.List;


/**
 * <p>
 * 客服表 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
@RequestMapping("/customerService")
public class CustomerServiceController {

    @Autowired
    private ICustomerServiceService iCustomerServiceService;
    @Autowired
    private ICustomerInfoService iCustomerInfoService;
    @Autowired
    private INoticeService iNoticeService;


    @RequestMapping("/login")

    public String checkCustomerEmailAndPsw(CustomerService customerService, HttpSession session, Model model) {


        String path = "";
        QueryWrapper<CustomerService> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("email", customerService.getEmail());
        queryWrapper.eq("password", customerService.getPassword());
        CustomerService cs = iCustomerServiceService.getOne(queryWrapper);
        System.out.println(555);
        System.out.println(session.getAttribute("cs"));
        // 两种方式会回到Index页面，第一种为Login页面登录，此时cs不为空即可；第二种为index页面中点击控制台，此时应查询session中是否有值，再回到index页面

        if (cs != null || session.getAttribute("cs") != null) {

            model.addAttribute("customerServiceNumber", iCustomerServiceService.count());
            model.addAttribute("customerInfoNumber", iCustomerInfoService.count());
            model.addAttribute("noticeNumber", iNoticeService.count());


            if (session.getAttribute("cs") == null) // 从Login页面登录index页面

                session.setAttribute("cs", cs);
            else // 控制台跳转到index页面
                cs = (CustomerService) session.getAttribute("cs");

            session.setAttribute("userRealName", cs.getRealname());
            path = "index";
        }
        else
        {

            session.setAttribute("msg", "用户名或密码错误！");
            path = "login";
        }
        return path;
    }

    @RequestMapping("/index")
    public String toCustomerServiceIndex(Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        IPage<CustomerService> iPage = new Page<>(current, size);
        iPage = iCustomerServiceService.page(iPage);
        List<CustomerService> list = iPage.getRecords();
        model.addAttribute("lists",list);
        return "workloadStatistics";
    }

    @RequestMapping("/update")
    public String customerServiceUpdate(Model model)
    {
        return "null";
    }

    @RequestMapping("/delete")
    public String customerServiceDelete(CustomerService customerService, Model model) {
        iCustomerServiceService.removeById(customerService.getCustomerserviceid());
        List<CustomerService> list = iCustomerServiceService.list();
        model.addAttribute("lists", list);
        return "customerService";
    }
}

