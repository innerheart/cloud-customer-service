package com.igeekhome.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeekhome.biz.IWorkQualityStatisticsService;
import com.igeekhome.pojo.CustomerService;
import com.igeekhome.pojo.WorkQualityStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * 工作质量统计 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
@RequestMapping("/workQualityStatistics")
public class WorkQualityStatisticsController {
    @Autowired
    private IWorkQualityStatisticsService iWorkQualityStatisticsService;

    @RequestMapping("/index")
    public String toWorkQualityStatisticsIndex(HttpSession session, Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        IPage<WorkQualityStatistics> iPage = new Page<>(current, size);
        iPage = iWorkQualityStatisticsService.page(iPage);
        List<WorkQualityStatistics> list = iPage.getRecords();
        model.addAttribute("lists", list);
        CustomerService customerService = (CustomerService) session.getAttribute("cs");
        model.addAttribute("pagecount", iPage.getPages());
        session.setAttribute("userRealName", customerService.getRealname());
        int[] a = new int[6];
        for (WorkQualityStatistics wqs: list
             ) {
            a[0] += wqs.getGoodreview();
            a[1] += wqs.getMediumreview();
            a[2] += wqs.getBadreview();
            a[3] += wqs.getNoreview();
            a[4] += wqs.getResolved();
            a[5] += wqs.getUnsolved();
        }
        model.addAttribute("a", a);
        return "workQualityStatistics";
    }
}

