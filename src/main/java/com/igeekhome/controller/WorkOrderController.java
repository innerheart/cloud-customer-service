package com.igeekhome.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeekhome.biz.IBlackListService;
import com.igeekhome.biz.ICustomerServiceService;
import com.igeekhome.biz.IWorkOrderReplyService;
import com.igeekhome.biz.IWorkOrderService;
import com.igeekhome.pojo.BlackList;
import com.igeekhome.pojo.CustomerService;
import com.igeekhome.pojo.WorkOrder;
import com.igeekhome.pojo.WorkOrderReply;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 工作订单 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
@RequestMapping("/workOrder")
public class WorkOrderController {
    @Autowired
    private IWorkOrderService iWorkOrderService;

    @Autowired
    private IWorkOrderReplyService iWorkOrderReplyService;


    //跳转到工单初始界面
    @RequestMapping("/index")
    public String index(Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size) {
        //初始化工单界面
        initWorkOrderService(model, current, size);
        return "workorder";
    }


    //跳转到工单详情页面，可以改变工单处理状态和添加客服回复
    @RequestMapping("/detail")
    public String update(Model model, WorkOrder workOrder, @RequestParam("current") Integer current, @RequestParam("size") Integer size) {
        // 根据id查询工单详细信息
        workOrder = this.iWorkOrderService.getById(workOrder.getId());
        //根据客服id查询所有对该工单的回复
        QueryWrapper<WorkOrderReply> queryWrapper = new QueryWrapper<WorkOrderReply>();
        queryWrapper.eq("customerserviceid", workOrder.getCustomerserviceid());
        List<WorkOrderReply> list = this.iWorkOrderReplyService.list(queryWrapper);

        model.addAttribute("workOrder", workOrder);
        model.addAttribute("workOrderReply", list);
        model.addAttribute("state", workOrder.getState());
        model.addAttribute("main", "工单详情");
        model.addAttribute("current", current);
        model.addAttribute("size", size);
        return "workorderdetail";
    }

    //更改工单详情并返回当前页面
    @RequestMapping(value = "/detailSubmit", method = RequestMethod.POST)
    public String updateSubmit(Model model,HttpServletRequest request, WorkOrder workOrder, @RequestParam("current") Integer current, @RequestParam("size") Integer size) {

        String workOrderReply1 = request.getParameter("workorderreply");
        String state = request.getParameter("state");

        //更改工单最后修改日期
        LocalDateTime dateTime = LocalDateTime.now();
        workOrder.setUpdatetime(dateTime);

        //将客服回复和工单状态更新到数据库
        WorkOrderReply workOrderReply = new WorkOrderReply();
        workOrderReply.setCustomerserviceid(workOrder.getCustomerserviceid());
        workOrderReply.setReplytime(dateTime);
        workOrderReply.setContent(workOrderReply1);
        iWorkOrderReplyService.save(workOrderReply);
        iWorkOrderService.updateById(workOrder);
        //回到初始界面
        initWorkOrderService(model, current, size);
        return "workorder";
    }

    //跳转到添加页面
    @RequestMapping("/add")
    public String add(Model model, WorkOrder workOrder, HttpSession session) {
        model.addAttribute("main", "工单添加");
        //传递当前操作的客服id
        CustomerService customerService = (CustomerService) session.getAttribute("cs");
        model.addAttribute("id", customerService.getCustomerserviceid());
        return "workorderadd";
    }

    //添加并返回工单首页
    @RequestMapping(value = "/addSubmit", method = RequestMethod.POST)
    public String addSubmit(Model model, WorkOrder workOrder, HttpSession session, @RequestParam("current") Integer current, @RequestParam("size") Integer size) {
        //更改工单最后修改日期和创建日期并保存到数据库
        LocalDateTime dateTime = LocalDateTime.now();
        workOrder.setCreatetime(dateTime);
        workOrder.setUpdatetime(dateTime);
        iWorkOrderService.save(workOrder);
        //返回工单初始化界面
        initWorkOrderService(model, current, size);
        return "workorder";
    }


    //查询
    @RequestMapping("/select")
    public String select(Model model, HttpServletRequest request, @RequestParam("current") Integer current, @RequestParam("size") Integer size) {
        //对查询信息添加到QueryWrapper
        QueryWrapper<WorkOrder> queryWrapper = new QueryWrapper<WorkOrder>();
        String state = request.getParameter("state");
        if (state != null && state != "")
            queryWrapper.eq("state", state);
        String priority = request.getParameter("priority");
        if (priority != null && priority != "")
            queryWrapper.eq("priority", priority);
        String worktype = request.getParameter("worktype");
        if (worktype != null && worktype != "")
            queryWrapper.eq("worktype", worktype);
        String channel = request.getParameter("channel");
        if (channel != null && channel != "")
            queryWrapper.eq("channel", channel);
        String customerserviceid = request.getParameter("customerserviceid");
        if (customerserviceid != null && customerserviceid != "")
            queryWrapper.eq("customerServiceId", customerserviceid);
        String customerid = request.getParameter("customerid");
        if (customerid != null && customerid != "")
            queryWrapper.eq("customerId", customerid);


        //分页加载查询结果工单界面
        IPage<WorkOrder> page = new Page<>(current, size);
        IPage<WorkOrder> page1 = this.iWorkOrderService.page(page, queryWrapper);

        List<WorkOrder> list = page1.getRecords();
        long pagesCount = page1.getPages();


        model.addAttribute("list", list);
        model.addAttribute("pagesCount", pagesCount);
        model.addAttribute("main", "工单查询结果");
        model.addAttribute("state", state);
        model.addAttribute("priority", priority);
        model.addAttribute("worktype", worktype);
        model.addAttribute("channel", channel);
        model.addAttribute("customerserviceid", customerserviceid);
        model.addAttribute("customerid", customerid);


        return "workorderselect";
    }

    //分页加载初始化工单界面
    public void initWorkOrderService(Model model, Integer current, Integer size) {

        IPage<WorkOrder> page = new Page<WorkOrder>(current, size);
        IPage<WorkOrder> page1 = this.iWorkOrderService.page(page);

        List<WorkOrder> list = page1.getRecords();
        long pagesCount = page1.getPages();


        model.addAttribute("list", list);
        model.addAttribute("pagesCount", pagesCount);
        model.addAttribute("main", "工单管理主页");

    }
}

