package com.igeekhome.controller;



import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.stereotype.Controller;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.igeekhome.biz.IAttendanceStatisticsService;
import com.igeekhome.pojo.AttendanceStatistics;
import com.igeekhome.pojo.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestParam;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 * 考勤表 前端控制器
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Controller
@RequestMapping("/attendanceStatistics")
public class AttendanceStatisticsController {


    @Autowired
    private IAttendanceStatisticsService iAttendanceStatisticsService;

    @RequestMapping("/index")
    public String toAttendanceStatisticsIndex(HttpSession session, Model model, @RequestParam("current") Integer current, @RequestParam("size") Integer size)
    {
        IPage<AttendanceStatistics> iPage = new Page<>(current, size);
        iPage = iAttendanceStatisticsService.page(iPage);
        List<AttendanceStatistics> list = iPage.getRecords();
        model.addAttribute("lists", list);
        CustomerService customerService = (CustomerService) session.getAttribute("cs");
        model.addAttribute("pagecount", iPage.getPages());
        session.setAttribute("userRealName", customerService.getRealname());

        return "attendanceStatistics";
    }
}

