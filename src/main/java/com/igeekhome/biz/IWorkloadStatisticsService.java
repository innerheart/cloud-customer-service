package com.igeekhome.biz;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.igeekhome.pojo.WorkloadStatistics;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 工作量统计 服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface IWorkloadStatisticsService extends IService<WorkloadStatistics> {
    public int getTotalMessageCountSum(QueryWrapper queryWrapper);
    public int getTotalSessionCountSum(QueryWrapper queryWrapper);
    public int getTotalEffectiveSessionCountSum(QueryWrapper queryWrapper);
    public int getTotalEndSessionCountSum(QueryWrapper queryWrapper);
    public int getTotalSessionTimeNum(QueryWrapper queryWrapper);
}
