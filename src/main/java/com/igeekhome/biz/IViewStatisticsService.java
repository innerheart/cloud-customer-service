package com.igeekhome.biz;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.igeekhome.pojo.ViewStatistics;


import java.util.List;
/**
 * <p>
 * 视图统计 服务类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
public interface IViewStatisticsService extends IService<ViewStatistics> {
    public int[] getPageViewCol(QueryWrapper queryWrapper);
    public int[] getVisitorNumCol(QueryWrapper queryWrapper);
    public int[] getVisitorCountCol(QueryWrapper queryWrapper);
}
