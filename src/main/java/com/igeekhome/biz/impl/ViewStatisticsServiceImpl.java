package com.igeekhome.biz.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.igeekhome.pojo.ViewStatistics;
import com.igeekhome.dao.ViewStatisticsMapper;
import com.igeekhome.biz.IViewStatisticsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
/**
 * <p>
 * 视图统计 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Service
public class ViewStatisticsServiceImpl extends ServiceImpl<ViewStatisticsMapper, ViewStatistics> implements IViewStatisticsService {
    @Override
    public int[] getPageViewCol(QueryWrapper queryWrapper) {
        if(queryWrapper == null) queryWrapper = new QueryWrapper<ViewStatistics>();
        return this.baseMapper.getPageViewCol(queryWrapper);
    }

    @Override
    public int[] getVisitorNumCol(QueryWrapper queryWrapper) {
        if(queryWrapper == null) queryWrapper = new QueryWrapper<ViewStatistics>();
        return this.baseMapper.getVisitorNumCol(queryWrapper);
    }

    @Override
    public int[] getVisitorCountCol(QueryWrapper queryWrapper) {
        if(queryWrapper == null) queryWrapper = new QueryWrapper<ViewStatistics>();
        return this.baseMapper.getVisitorCountCol(queryWrapper);
    }
}
