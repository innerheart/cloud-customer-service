package com.igeekhome.biz.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.igeekhome.pojo.WorkloadStatistics;
import com.igeekhome.dao.WorkloadStatisticsMapper;
import com.igeekhome.biz.IWorkloadStatisticsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 工作量统计 服务实现类
 * </p>
 *
 * @author ${author}
 * @since 2021-03-16
 */
@Service
public class WorkloadStatisticsServiceImpl extends ServiceImpl<WorkloadStatisticsMapper, WorkloadStatistics> implements IWorkloadStatisticsService {
    @Override
    public int getTotalMessageCountSum(QueryWrapper queryWrapper) {
        if(queryWrapper == null) queryWrapper = new QueryWrapper<WorkloadStatistics>();
        return this.baseMapper.getTotalMessageCountSum(queryWrapper);
    }

    @Override
    public int getTotalSessionCountSum(QueryWrapper queryWrapper) {
        if(queryWrapper == null) queryWrapper = new QueryWrapper<WorkloadStatistics>();
        return this.baseMapper.getTotalSessionCountSum(queryWrapper);
    }

    @Override
    public int getTotalEffectiveSessionCountSum(QueryWrapper queryWrapper) {
        if(queryWrapper == null) queryWrapper = new QueryWrapper<WorkloadStatistics>();
        return this.baseMapper.getTotalEffectiveSessionCountSum(queryWrapper);
    }

    @Override
    public int getTotalEndSessionCountSum(QueryWrapper queryWrapper) {
        if(queryWrapper == null) queryWrapper = new QueryWrapper<WorkloadStatistics>();
        return this.baseMapper.getTotalEndSessionCountSum(queryWrapper);
    }

    @Override
    public int getTotalSessionTimeNum(QueryWrapper queryWrapper) {
        if(queryWrapper == null) queryWrapper = new QueryWrapper<WorkloadStatistics>();
        return this.baseMapper.getTotalSessionTimeNum(queryWrapper);
    }
}
