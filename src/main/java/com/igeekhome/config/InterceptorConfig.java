package com.igeekhome.config;


import com.igeekhome.util.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration  //当设置了Configuration 在sping boot 启动后会在类路径下扫描有没有添加当Configuration，有，这个文件是配制文件
public class InterceptorConfig implements WebMvcConfigurer {
    // 拦截设置
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        registry.addInterceptor(new Filter())// 添加拦截器对像
                .addPathPatterns("/**") //拦截哪些请求
                .excludePathPatterns("/assets/**", "/css/**", "/", "/login", "/fonts/**", "/images/**", "/js/**", "/customerService/login", "/signUp", "/RegisterToIndex"); //哪些请求不拦截

    }
}
