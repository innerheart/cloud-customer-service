package com.igeekhome.util;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 这是个拦截器
 */
public class Filter implements HandlerInterceptor {

    /**
     *     // 请求之前会调用这里，它是个实现了HandlerInterceptor接口的拦截器类，完成判断用户是否登录
     * @param request 请求对像
     * @param response  响应对象
     * @param handler
     * @return
     * @throws Exception
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HttpSession session = request.getSession();

        System.out.println("cs = " + session.getAttribute("cs"));
        System.out.println("url = " + request.getRequestURI());
        System.out.println("path = " + request.getPathInfo() + "\n\n");
        if(session.getAttribute("cs") == null)
        {
            session.setAttribute("msg","你要正常登录才可以访问");
          //  return "redirect:/page-login";
            response.sendRedirect("/login");
        }


        return true;
    }
// 请求控制器之后，
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

    }

    // 在模板页操作之前
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }

}
