package com.igeekhome.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.igeekhome.biz.IWorkloadStatisticsService;
import com.igeekhome.pojo.WorkloadStatistics;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class WorkloadStatisticsMapperTest {

    @Autowired
    private WorkloadStatisticsMapper workloadStatisticsMapper;

    @Test
    void Mytest()
    {
        int i = workloadStatisticsMapper.getTotalMessageCountSum(new QueryWrapper<WorkloadStatistics>());
        System.out.println(i);
    }
}
