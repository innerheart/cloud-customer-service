package com.igeekhome.biz;

import com.igeekhome.pojo.Notice;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class NoticeServiceTest {

    @Autowired
    private INoticeService iNoticeService;

    @Test
    void NoticeServiceSave()
    {
        Notice notice = new Notice();
        notice.setCreatorid(11);
        iNoticeService.save(notice);
    }
}
